<?php
/**
 * Deze plugin is verantwoordelijk voor het registreren van bijzondere post-types voor de website van Genootschap Amstelodamum
 *
 * @link              http://www.somtijds.nl
 * @since             1.0.0
 * @package           Amstelodamum_Post Types
 *
 * @wordpress-plugin
 * Plugin Name:       Amstelodamum Post Types
 * Plugin URI:        https://bitbucket.org/retrorism/amstelodamum-post-types
 * Description:       Deze plugin maakt het mogelijk om te zoeken op het digitale archief op de website van Genootschap Amstelodamum
 * Version:           1.0.0
 * Author:            Willem Prins
 * Author URI:        http://www.somtijds.nl
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       amstelodamum
 */

namespace Amstelodamum\PostTypes;
use function \has_action;
use function \add_action;
use function \register_post_type;
use function \date;
use function \register_widget;


/**
* Register post types
*/

if (!has_action('amstelodamum_post_types')) {
    function register_amstelodamum_post_types()
    {
            register_post_type('publication', array(
            'labels' => array(
                'name' => _x('Publicaties', 'Post Type General Name', 'amstelodamum'),
                'singular_name' => _x('Publicatie', 'Post Type Singular Name', 'amstelodamum'),
                'menu_name' => __('Publicaties', 'amstelodamum'),
                'name_admin_bar' => __('Publicatie', 'amstelodamum'),
                'all_items' => __('Alle publicaties', 'amstelodamum'),
                'add_new_item' => __('Nieuwe publicatie', 'amstelodamum'),
                'add_new' => __('Nieuwe publicatie', 'amstelodamum'),
                'new_item' => __('Nieuwe publicatie', 'amstelodamum')
            ),
            'public' => true,
            'has_archive' => false,
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => false,
            'rewrite' => false,
            'query_var' => false,
            'delete_with_user' => false,
            'supports' => array('title', 'editor','thumbnail'),
        ));
        register_post_type('activity', array(
            'labels' => array(
                'name' => _x('Activiteiten', 'Post Type General Name', 'amstelodamum'),
                'singular_name' => _x('Activiteit', 'Post Type Singular Name', 'amstelodamum'),
                'menu_name' => __('Activiteiten', 'amstelodamum'),
                'name_admin_bar' => __('Activiteit', 'amstelodamum'),
                'all_items' => __('Alle activiteiten', 'amstelodamum'),
                'add_new_item' => __('Nieuwe activiteit', 'amstelodamum'),
                'add_new' => __('Nieuwe activiteit', 'amstelodamum'),
                'new_item' => __('Nieuwe activiteit', 'amstelodamum')
            ),

            'public' => true,
            'has_archive' => false,
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => false,
            'rewrite' => false,
            'query_var' => false,
            'delete_with_user' => false,
            'supports' => array('title', 'editor', 'thumbnail'),
        ));
    }

    add_action('init', __NAMESPACE__ .'\\register_amstelodamum_post_types');
}



/**
 * Add Activity_Widget widget
 */
class Activity_Widget extends \WP_Widget {

    function __construct() {
        parent::__construct(
            'activity_widget', // Base ID
            __( 'Activiteiten', 'amstelodamum' ), // Name
            array( 'description' => __( 'Activity Widget', 'amstelodamum' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
        }

        $today = date( 'Ymd' );

        $query_args = array(
            'post_type' => 'activity',
            'posts_per_page' => -1,
            'meta_key' => 'start_date',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key' => 'start_date',
                    'compare' => '>=',
                    'value' => $today,
                ),
                array(
                    'key' => 'end_date',
                    'compare' => '>=',
                    'value' => $today,
                ),
            ),
        );

        $the_query = new \WP_Query( $query_args );

        while ($the_query -> have_posts()) : $the_query -> the_post();
            echo "<a href=\"" , the_permalink() , "\" >" , the_title() , "</a>";

            // start en einddatum hetzelfde
            if ( get_field( 'start_date' ) == get_field( 'end_date' ) OR trim( get_field( 'end_date' ) ) == false ) {
                $dateformatstring = "l j F Y";
                $unixtimestamp = strtotime(get_field('start_date'));
                echo "<div>" , ucfirst( date_i18n( $dateformatstring, $unixtimestamp ) ) , "</div>";

                // starttijd niet leeg
                if ( ! trim( get_field( 'start_time' ) ) == false ) {
                    // start en eindtijd hetzelfde
                    if ( get_field( 'start_time' ) == get_field( 'end_time' ) OR trim( get_field( 'end_time' ) ) == false ) {
                        echo "<div>Om " , the_field( 'start_time' ) , " uur</div>";
                    } else {
                        echo "<div>Van " , the_field( 'start_time' ) , " tot " , the_field( 'end_time' ) , " uur</div>";
                    }
                }

                // start en einddatum zelfde maand en jaar
            } else  if ( substr( get_field( 'start_date' ), 0, 6 ) == substr( get_field( 'end_date' ), 0, 6 )  ) {
                $dateformatstring = "j";
                $unixtimestamp = strtotime(get_field('start_date'));
                echo "<div>" , date_i18n( $dateformatstring, $unixtimestamp ), " - ";
                $dateformatstring = "j F Y";
                $unixtimestamp = strtotime(get_field('end_date'));
                echo date_i18n( $dateformatstring, $unixtimestamp ) , "</div>";

                // start en einddatum zelfde jaar
            } else  if ( substr( get_field( 'start_date' ), 0, 4 ) == substr( get_field( 'end_date' ), 0, 4 )  ) {
                $dateformatstring = "j F";
                $unixtimestamp = strtotime(get_field('start_date'));
                echo "<div>" , date_i18n( $dateformatstring, $unixtimestamp ), " - ";
                $dateformatstring = "j F Y";
                $unixtimestamp = strtotime(get_field('end_date'));
                echo date_i18n( $dateformatstring, $unixtimestamp ) , "</div>";

                // start en einddatum verschillend jaar
            } else {
                $dateformatstring = "j F Y";
                $unixtimestamp = strtotime(get_field('start_date'));
                echo "<div>" , date_i18n( $dateformatstring, $unixtimestamp ), " - ";
                $dateformatstring = "j F Y";
                $unixtimestamp = strtotime(get_field('end_date'));
                echo date_i18n( $dateformatstring, $unixtimestamp ) , "</div>";
            }

            if ( ! trim( get_field( 'location_name' ) ) == false ) {
                echo "<div>" , the_field('location_name') , "</div>";
            }

            if ( ( ! trim( get_field( 'location_city' ) ) == false ) AND ( get_field( 'location_city' ) !== 'Amsterdam' ) ) {
                echo "<div>" , the_field( 'location_city' ) , "</div>";
            }

            if ( ! trim( get_field( 'frontpage_text' ) ) == false ) {
                echo the_field( 'frontpage_text' );
            } else {
                echo "<p></p>";
            }

        endwhile;

        wp_reset_query();

        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'amstelodamum' );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

} // Class Activity_Widget

// Register Activity_Widget widget
function register_activity_widget() {
    register_widget( __NAMESPACE__ . '\\Activity_Widget' );
}

add_action( 'widgets_init', __NAMESPACE__ . '\\register_activity_widget' );


/**
 * Add Publication_Widget widget
 */
class Publication_Widget extends \WP_Widget {

    function __construct() {
        parent::__construct(
            'publication_widget', // Base ID
            __( 'Publicaties', 'amstelodamum' ), // Name
            array( 'description' => __( 'Publication Widget', 'amstelodamum' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
        }

        $today = date( 'Ymd' );

        $query_args = array(
            'post_type' => 'publication',
            'posts_per_page' => 2,
            'order' => 'DESC',
        );

        $the_query = new \WP_Query( $query_args );

        while ($the_query -> have_posts()) : $the_query -> the_post();

            $publication_cover = \get_field('publication_cover');

            if ( $publication_cover ) :
                echo '<div class="publication-widget-cover">', '<a href="', the_permalink() , '" >' , wp_get_attachment_image( $publication_cover, 'small' ), '</div>';
            endif;

        endwhile;

        \wp_reset_query();

        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'amstelodamum' );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

} // Class Publication_Widget

// Register Publication_Widget widget
function register_publication_widget() {
    \register_widget( __NAMESPACE__ .'\\Publication_Widget' );
}

\add_action( 'widgets_init', __NAMESPACE__ .'\\register_publication_widget' );
